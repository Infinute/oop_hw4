import ntu.csie.oop13spring.*;

public class POOPlayground extends POOArena{
    private int[] cat_p = new int[2];
    private int[] dog_p = new int[2];
    private boolean init = false;
    public final int MAX_X = 10;
    public final int MAX_Y = 10;  
    POOCat cat = new POOCat();
    POODog dog = new POODog();
    private int round = 0;
    public boolean fight(){
	if (init == false){
	    System.out.println("Game Start!");	    
	    POOPet[] two_pets = super.getAllPets();
	    cat = (POOCat)two_pets[0];
	    dog = (POODog)two_pets[1];
	    cat.init();
	    dog.init();
	    cat_p[0] = 0;
	    cat_p[1] = 5;
	    dog_p[0] = 10;
	    dog_p[1] = 5;
	    init = true;
	}
	round++;
	System.out.printf("-----Round %d------\n", round);
	try {
	    Thread.sleep(500);
	} catch(InterruptedException e) {
	    Thread.currentThread().interrupt();
	}
	System.out.printf("Cat's pos:(%d,%d) | Dog's pos:(%d,%d)\n", cat_p[0],cat_p[1],dog_p[0],dog_p[1]);
	try {
	    Thread.sleep(500);
	} catch(InterruptedException e) {
	    Thread.currentThread().interrupt();
	}
	System.out.printf("Cat's status-> hp:%d mp:%d | Dog's status-> hp:%d mp:%d\n", cat.GetHP(),cat.GetMP(),dog.GetHP(),dog.GetMP());	
	try {
	    Thread.sleep(1000);
	} catch(InterruptedException e) {
	    Thread.currentThread().interrupt();
	}
	System.out.println("-----------------");	
	System.out.println("Cat's turn...");	
	try {
	    Thread.sleep(1000);
	} catch(InterruptedException e) {
	    Thread.currentThread().interrupt();
	}
	cat.act(this);
	try {
	    Thread.sleep(1000);
	} catch(InterruptedException e) {
	    Thread.currentThread().interrupt();
	}
	if (dog.GetHP()==0) {
	    System.out.println("~~~~~~~Cat Wins!~~~~~~~");    
	    return false;
	}
	System.out.println("Dog's turn...");	
	try {
	    Thread.sleep(1000);
	} catch(InterruptedException e) {
	    Thread.currentThread().interrupt();
	}
	dog.act(this);
	try {
	    Thread.sleep(1000);
	} catch(InterruptedException e) {
	    Thread.currentThread().interrupt();
	}
	if (cat.GetHP()==0) {
	    System.out.println("~~~~~~~Dog Wins!~~~~~~~");
	    return false;
	}
	return true;
    }
    public void show(){
	try {
	    Thread.sleep(2000);
	} catch(InterruptedException e) {
	    Thread.currentThread().interrupt();
	}
    }
	public POONavigator GetPosition(POOPet p){
		POONavigator c = (POONavigator)getPosition(p);
		return c;
	}
	public POOCoordinate getPosition(POOPet p){
		POONavigator c = new POONavigator();
		if (p == cat)
			c.set(cat_p[0],cat_p[1]);
		else if (p == dog)
			c.set(dog_p[0],dog_p[1]);
		return (POOCoordinate)c;
    }
    
	public POONavigator getDistance(POOPet p){
		POONavigator c = new POONavigator();
		if (p == cat)
			c.set(dog_p[0]-cat_p[0],dog_p[1]-cat_p[1]);
		else if (p == dog)
			c.set(cat_p[0]-dog_p[0],cat_p[1]-dog_p[1]);
		return c;
    }
	
	public void set_pos(POONavigator n, int i){
		if (i==1){
			System.out.printf("Cat's pos:(%d,%d)->", cat_p[0],cat_p[1]);	
			cat_p[0] = n.getx();
			cat_p[1] = n.gety();
			System.out.printf("(%d,%d)\n", cat_p[0],cat_p[1]);	
		} else {
			System.out.printf("Dog's pos:(%d,%d)->", dog_p[0],dog_p[1]);
			dog_p[0] = n.getx();
			dog_p[1] = n.gety();
			System.out.printf("(%d,%d)\n", dog_p[0],dog_p[1]);
		}
	}
}
