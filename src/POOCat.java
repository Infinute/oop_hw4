import ntu.csie.oop13spring.*;

import java.util.Random;

public class POOCat extends POOPet{
    int dir_x;
    int dir_y;
    int move_mode;//0 = attack, 1 = run away.
    boolean init = false;
    public void init(){
    	if (init == false){
        	setHP(15);
        	setMP(10);
        	setAGI(6);
        	setName("Cat");
	}
	init = true;
    }
    
    static protected final boolean CheckHP(int _HP){
        return checkHP(_HP);
    }

    static protected final boolean CheckMP(int _MP){
        return checkMP(_MP);
    }
    
    static protected final boolean CheckAGI(int _AGI){
        return checkAGI(_AGI);
    }
    
    protected final boolean SetHP(int HP){
        return setHP(HP);       
    }

    protected final boolean SetMP(int MP){
        return setMP(MP);        
    }
    
    protected final boolean SetAGI(int AGI){
        return setAGI(AGI);         
    }
    
    protected final boolean SetName(String name){
	return setName(name);
    }
    
    protected final String GetName(){
        return getName();
    }
    
    protected final int GetAGI(){
        return getAGI();
    }

    protected final int GetMP(){
        return getMP();
    }
    
    protected final int GetHP(){
        return getHP();
    }
    protected POOAction act(POOArena arena){
		POOAction r = new POOAction();
		POOPlayground Arena = (POOPlayground)arena;
		/*public final class POOAction{
			POOSkill skill;
			POOPet dest;
		}*/
		//POOPet[] allpets = new POOPet[0];
		POOPet[] allpets = arena.getAllPets();
		POODog dog = (POODog)allpets[1];
		POONavigator distance =	Arena.getDistance(this);
		//POONavigator pos = (POONavigator)arena.getPosition(this);
		int dis=0;
		if (distance.getx()<0)
			dis-=distance.getx();
		else
			dis+=distance.getx();
		if (distance.gety()<0)
			dis-=distance.gety();
		else
			dis+=distance.gety();	
		if (dis>=7){
			TakeARest skill = new TakeARest();
			skill.Act(this);
		}
		else if (dis<=2 && GetMP()>=2){
			move_mode = 0;
			move(arena);
			try {
			    Thread.sleep(1000);
			} catch(InterruptedException e) {
			    Thread.currentThread().interrupt();
			}
			HitAndRun skill = new HitAndRun();
			skill.Act(dog);
			move_mode = 1;
			try {
			    Thread.sleep(1000);
			} catch(InterruptedException e) {
			    Thread.currentThread().interrupt();
			}
			move(arena);
			System.out.printf("Cat's mp: %d -> %d\n", GetMP(),GetMP()-2);	
			SetMP(GetMP()-2);
		}
		else {
			Random ran = new Random();
			move_mode = ran.nextInt(2);
			move(arena);
			try {
			    Thread.sleep(1000);
			} catch(InterruptedException e) {
			    Thread.currentThread().interrupt();
			}
			if (move_mode == 0){
				RecklesslyAttack skill = new RecklesslyAttack();
				skill.Act(dog);
			} else {
				TakeARest skill = new TakeARest();
				skill.Act(this);
			}
		}
		return r;
    }

    protected POOCoordinate move(POOArena arena){
		POOPlayground Arena = (POOPlayground)arena;
		POONavigator pos = Arena.GetPosition(this);
		POONavigator dis = Arena.getDistance(this);
		if ( move_mode == 0 ){
			if (dis.getx()==0){
				if (dis.gety()>0)
					pos.set(pos.getx(),pos.gety()+dis.gety()-1);
				else
					pos.set(pos.getx(),pos.gety()+dis.gety()+1);
			} else if (dis.gety()==0){
				if (dis.getx()>0)
					pos.set(pos.getx()+dis.getx()-1,pos.gety());
				else
					pos.set(pos.getx()+dis.getx()+1,pos.gety());			    
			} else if (dis.getx()>0){
				if (dis.gety()>0)
					pos.set(pos.getx()+dis.getx()-1,pos.gety()+dis.gety());
				else
					pos.set(pos.getx()+dis.getx()-1,pos.gety()+dis.gety());			    
			} else if (dis.getx()<0){
				if (dis.gety()>0)
					pos.set(pos.getx()+dis.getx(),pos.gety()+dis.gety()-1);
				else
					pos.set(pos.getx()+dis.getx(),pos.gety()+dis.gety()+1);	
			}
		} else if ( move_mode == 1){
		    	int section; 
		    	/* 
		    	 * 123
		    	 * 456
		    	 * 789
		    	 */
		    	int agi = 6;
		    	int x=0,y=0;
		    	if (pos.getx()<3)
		    	    section = 1;
		    	else if (pos.getx()>7)
		    	    section = 3;
		    	else
		    	    section = 2;
		    	if (pos.gety()<3)
		    	    section += 0;
		    	else if (pos.gety()>7)
		    	    section += 6;
		    	else
		    	    section += 3;
		    	if (section == 1){
		    	    if ( dis.getx()<=dis.gety() )
		    		pos.set(pos.getx()+6,pos.gety());
		    	    else
		    		pos.set(pos.getx(),pos.gety()+6);
		    	} else if (section == 2) {
		    	    if ( dis.getx() <= 0 ){
		    		for (int i = 1; i <= agi; i++){
		    		    if (pos.getx()+i == 10){
		    			x = i;
		    			y = i - agi;
		    			break;
		    		    }
		    		}
		    		if (pos.getx()+y < 0)
		    		    pos.set(pos.getx()+x,0);
		    		else
		    		    pos.set(pos.getx()+x,pos.gety()+y);
		    	    } else {
		    		for (int i = 1; i <= agi; i++){
		    		    if (pos.getx()-i == 0){
		    			x = -i;
		    			y = i - agi;
		    			break;
		    		    }
		    		}
		    		if (pos.gety()+y < 0)
		    		    pos.set(pos.getx()+x,0);
		    		else
		    		    pos.set(pos.getx()+x,pos.gety()+y);
		    	    }
		    	} else if (section == 3){
		    	    if ( -dis.getx() <= dis.gety() )
		    		pos.set(pos.getx()-6,pos.gety());
		    	    else
		    		pos.set(pos.getx(),pos.gety()+6);
		    	} else if (section == 4) {
		    	    if ( dis.gety() <= 0 ){
		    		for (int i = 1; i <= agi; i++){
		    		    if (pos.gety()+i == 10){
		    			y = i;
		    			x = i - agi;
		    			break;
		    		    }
		    		}
		    		if (pos.getx()+x < 0)
		    		    pos.set(0,pos.gety()+y);
		    		else
		    		    pos.set(pos.getx()+x,pos.gety()+y);
		    	    } else {
		    		for (int i = 1; i <= agi; i++){
		    		    if (pos.gety()-i == 0){
		    			y = -i;
		    			x = i - agi;
		    			break;
		    		    }
		    		}
		    		if (pos.getx()+x < 0)
		    		    pos.set(0,pos.gety()+y);
		    		else
		    		    pos.set(pos.getx()+x,pos.gety()+y);
		    	    }
		    	} else if (section == 5) {
		    	    if (dis.getx()>=0)
		    		x = -3;
		    	    else
		    		x = 3;
		    	    if (dis.gety()>=0)
		    		y = -3;
		    	    else
		    		y = 3;	
	    		    pos.set(pos.getx()+x,pos.gety()+y);
		    	} else if (section == 6) {
		    	    if ( dis.gety() <= 0 ){
		    		for (int i = 1; i <= agi; i++){
		    		    if (pos.gety()+i == 10){
		    			y = i;
		    			x = agi - i;
		    			break;
		    		    }
		    		}
		    		if (pos.getx()+x > 10)
		    		    pos.set(10,pos.gety()+y);
		    		else
		    		    pos.set(pos.getx()+x,pos.gety()+y);
		    	    } else {
		    		for (int i = 1; i <= agi; i++){
		    		    if (pos.gety()-i == 0){
		    			y = -i;
		    			x = agi - i;
		    			break;
		    		    }
		    		}
		    		if (pos.getx()+x > 10)
		    		    pos.set(10,pos.gety()+y);
		    		else
		    		    pos.set(pos.getx()+x,pos.gety()+y);
		    	    }
		    	} else if (section == 7){
		    	    if ( dis.getx()<= -dis.gety() )
		    		pos.set(pos.getx()+6,pos.gety());
		    	    else
		    		pos.set(pos.getx(),pos.gety()-6);		    	    
		    	} else if (section == 8) {
		    	    if ( dis.getx() <= 0 ){
		    		for (int i = 1; i <= agi; i++){
		    		    if (pos.getx()+i == 10){
		    			x = i;
		    			y = agi - i;
		    			break;
		    		    }
		    		}
		    		if (pos.getx()+y > 10)
		    		    pos.set(pos.getx()+x,10);
		    		else
		    		    pos.set(pos.getx()+x,pos.gety()+y);
		    	    } else {
		    		for (int i = 1; i <= agi; i++){
		    		    if (pos.getx()-i == 0){
		    			x = -i;
		    			y = agi - i;
		    			break;
		    		    }
		    		}
		    		if (pos.gety()+y > 10)
		    		    pos.set(pos.getx()+x,10);
		    		else
		    		    pos.set(pos.getx()+x,pos.gety()+y);
		    	    }
		    	} else if (section == 9){
		    	    if ( dis.getx() >= dis.gety() )
		    		pos.set(pos.getx()-6,pos.gety());
		    	    else
		    		pos.set(pos.getx(),pos.gety()-6);
		    	}
		}
		Arena.set_pos(pos,1);
		return (POOCoordinate) pos;
    }
    
}

class HitAndRun extends POOSkill{
    public void Act(POODog pet){
        int hp = pet.GetHP();
        Random ran = new Random();
        int attack = ran.nextInt(3)+1;
        System.out.println("Cat attacks and run away!");
	try {
	    Thread.sleep(500);
	} catch(InterruptedException e) {
	    Thread.currentThread().interrupt();
	}
	System.out.printf("Dog's hp: %d -> ", hp);			
        if (hp - attack > 0)
            pet.SetHP(hp - attack);
        else
            pet.SetHP(0);
	System.out.printf("%d\n", pet.GetHP());
	}
    public void act(POOPet pet){
		Act((POODog)pet);
    }
}

class RecklesslyAttack extends POOSkill{
    public void Act(POODog pet){
        int hp = pet.GetHP();
        System.out.println("Cat attacks madly!");	
	try {
	    Thread.sleep(500);
	} catch(InterruptedException e) {
	    Thread.currentThread().interrupt();
	}
	System.out.printf("Dog's hp: %d -> ", hp);	
	if (hp - 3 > 0)
            pet.SetHP(hp - 3);
        else
            pet.SetHP(0);     
	System.out.printf("%d\n", pet.GetHP());			
    }
    public void act(POOPet pet){
		Act((POODog)pet);
    }
}

class TakeARest extends POOSkill{
    public void Act(POOCat pet){
	System.out.println("Cat is resting.");	
        int hp = pet.GetHP();
	try {
	    Thread.sleep(500);
	} catch(InterruptedException e) {
	    Thread.currentThread().interrupt();
	}
	System.out.printf("Cat's hp: %d -> ", hp);	
        if (hp < 15)
            pet.SetHP(hp + 1);
	System.out.printf("%d\n", pet.GetHP());
    }
    public void act(POOPet pet){
	Act((POOCat)pet);
    }
}