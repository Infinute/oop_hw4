import ntu.csie.oop13spring.*;;

public class POONavigator extends POOCoordinate{
    public boolean set(int x, int y){
	this.x = x;
	this.y = y;
	return true;
    }   
    public int getx(){
	return x;
    }
    public int gety(){
	return y;
    }
    public boolean equals(POOCoordinate other){
	return (other == this);
    }
    
}
