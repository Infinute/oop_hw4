import java.util.Random;

import ntu.csie.oop13spring.*;

public class POODog extends POOPet{
    boolean init = false;
    public void init(){
    	if (init == false){
        	setHP(20);
        	setMP(8);
        	setAGI(4);
        	setName("Dog");
	}
	init = true;
    }
    int move_mode; //0 = move, 1 = attack, 2 = rush
    
    static protected final boolean CheckHP(int _HP){
        return checkHP(_HP);
    }

    static protected final boolean CheckMP(int _MP){
        return checkMP(_MP);
    }
    
    static protected final boolean CheckAGI(int _AGI){
        return checkAGI(_AGI);
    }
    
    protected final boolean SetHP(int HP){
        return setHP(HP);       
    }

    protected final boolean SetMP(int MP){
        return setMP(MP);        
    }
    
    protected final boolean SetAGI(int AGI){
        return setAGI(AGI);         
    }
    
    protected final boolean SetName(String name){
	return setName(name);
    }
    
    protected final String GetName(){
        return getName();
    }
    
    protected final int GetAGI(){
        return getAGI();
    }

    protected final int GetMP(){
        return getMP();
    }
    
    protected final int GetHP(){
        return getHP();
    }
    protected POOAction act(POOArena arena){
	POOAction r = new POOAction();
	POOPlayground Arena = (POOPlayground)arena;
	/*public final class POOAction{
	    POOSkill skill;
	    POOPet dest;
	}*/
	//POOPet[] allpets = new POOPet[0];
	POOPet[] allpets = arena.getAllPets();
	POOCat cat = (POOCat)allpets[0];
	POONavigator distance =	Arena.getDistance(this);
	int dis=0;
	if (distance.getx()<0)
		dis-=distance.getx();
	else
		dis+=distance.getx();
	if (distance.gety()<0)
		dis-=distance.gety();
	else
		dis+=distance.gety();	
	if (dis>=7){
	    	move_mode = 0;
		move(arena);
	} else if (dis==6 && GetMP()>=2){
	    	move_mode = 2;
	    	move(arena);
		try {
		    Thread.sleep(1000);
		} catch(InterruptedException e) {
		    Thread.currentThread().interrupt();
		}
	        System.out.println("Dog rushs into Cat!");
	    	move_mode = 1;
		try {
		    Thread.sleep(1000);
		} catch(InterruptedException e) {
		    Thread.currentThread().interrupt();
		}
	    	move(arena);
		try {
		    Thread.sleep(1000);
		} catch(InterruptedException e) {
		    Thread.currentThread().interrupt();
		}
	    	Rush skill = new Rush();
	    	skill.Act(cat);
	} else if (dis==6){
	    move_mode = 0;
	    move(arena);
	} else {
	    move_mode = 1;
	    move(arena);
		try {
		    Thread.sleep(1000);
		} catch(InterruptedException e) {
		    Thread.currentThread().interrupt();
		}	    
	    Attack skill = new Attack();
	    skill.Act(cat);	    
	}   
	
	return r;
    }

    protected POOCoordinate move(POOArena arena){
	POOPlayground Arena = (POOPlayground)arena;
	POONavigator pos = Arena.GetPosition(this);
	POONavigator dis = Arena.getDistance(this);	
	if ( move_mode == 0 ){
		if (dis.getx()<2 && dis.getx()>-2){
			if (dis.gety()>0)
				pos.set(pos.getx(),pos.gety()+4);
			else
				pos.set(pos.getx(),pos.gety()-4);
		} else if (dis.gety()<2 && dis.gety()>-2){
			if (dis.getx()>0)
				pos.set(pos.getx()+4,pos.gety());
			else
				pos.set(pos.getx()-4,pos.gety());			    
		} else if (dis.getx()>0){
			if (dis.gety()>0)
				pos.set(pos.getx()+2,pos.gety()+2);
			else
				pos.set(pos.getx()+2,pos.gety()-2);			    
		} else if (dis.getx()<0){
			if (dis.gety()>0)
				pos.set(pos.getx()-2,pos.gety()+2);
			else
				pos.set(pos.getx()-2,pos.gety()-2);	
		} 
	} else if ( move_mode == 1 ){
		if (dis.getx()==0){
			if (dis.gety()>0)
				pos.set(pos.getx(),pos.gety()+dis.gety()-1);
			else
				pos.set(pos.getx(),pos.gety()+dis.gety()+1);
		} else if (dis.gety()==0){
			if (dis.getx()>0)
				pos.set(pos.getx()+dis.getx()-1,pos.gety());
			else
				pos.set(pos.getx()+dis.getx()+1,pos.gety());			    
		} else if (dis.getx()>0){
			if (dis.gety()>0)
				pos.set(pos.getx()+dis.getx()-1,pos.gety()+dis.gety());
			else
				pos.set(pos.getx()+dis.getx()-1,pos.gety()+dis.gety());			    
		} else if (dis.getx()<0){
			if (dis.gety()>0)
				pos.set(pos.getx()+dis.getx(),pos.gety()+dis.gety()-1);
			else
				pos.set(pos.getx()+dis.getx(),pos.gety()+dis.gety()+1);	
		}
	} else if ( move_mode == 2 ){
		if (dis.getx()<=1 && dis.getx()>=-1){
			if (dis.gety()>0)
				pos.set(pos.getx(),pos.gety()+dis.gety()-2);
			else
				pos.set(pos.getx(),pos.gety()+dis.gety()+2);
		} else if (dis.getx()<=-1 && dis.getx()>=-1){
			if (dis.getx()>0)
				pos.set(pos.getx()+dis.getx()-2,pos.gety());
			else
				pos.set(pos.getx()+dis.getx()+2,pos.gety());			    
		} else if (dis.getx()>1){
			pos.set(pos.getx()+dis.getx()-2,pos.gety()+dis.gety());		    
		} else if (dis.getx()<-1){
			pos.set(pos.getx()+dis.getx()+2,pos.gety()+dis.gety()-1);	
		}	
	}
	Arena.set_pos(pos,0);
	return (POOCoordinate) pos;
    }
}

class Rush extends POOSkill{
    public void Act(POOCat pet){
        int hp = pet.GetHP();
        Random ran = new Random();
        int attack = ran.nextInt(4)+3;
	try {
	    Thread.sleep(500);
	} catch(InterruptedException e) {
	    Thread.currentThread().interrupt();
	}
	System.out.printf("Cat's hp: %d -> ", hp);	
        if (hp - attack > 0)
            pet.SetHP(hp - attack);
        else
            pet.SetHP(0);
	System.out.printf("%d\n", pet.GetHP());	
    }
    public void act(POOPet pet){
	Act((POOCat)pet);
    }
}

class Attack extends POOSkill{
    public void Act(POOCat pet){
        int hp = pet.GetHP();
        System.out.println("Dog attacks Cat!");
	try {
	    Thread.sleep(500);
	} catch(InterruptedException e) {
	    Thread.currentThread().interrupt();
	}
	System.out.printf("Cat's hp: %d -> ", hp);
        if (hp - 4 > 0)
            pet.SetHP(hp - 4);
        else
            pet.SetHP(0);  
	System.out.printf("%d\n", pet.GetHP());	
    }
    public void act(POOPet pet){
	Act((POOCat)pet);
    }
}